EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev "v0.3.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CO2Mofetten:RJ45SensorCableConnector J1
U 1 1 5E39CEBA
P 7200 3600
F 0 "J1" H 6870 3604 50  0000 R CNN
F 1 "RJ45SensorCableConnector" H 6870 3695 50  0000 R CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 7200 3625 50  0001 C CNN
F 3 "~" V 7200 3625 50  0001 C CNN
	1    7200 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	5500 3500 5500 3550
Wire Wire Line
	5500 3550 5200 3550
Wire Wire Line
	6800 3700 6400 3700
Wire Wire Line
	5600 3700 5600 3450
Wire Wire Line
	5600 3450 5200 3450
Wire Wire Line
	6800 3800 6800 3900
Connection ~ 6800 3900
Wire Wire Line
	6800 3900 6800 4000
Wire Wire Line
	6800 3600 6750 3600
Wire Wire Line
	6750 3600 6750 3800
Wire Wire Line
	6750 3900 6800 3900
Wire Wire Line
	6650 4900 5950 4900
Wire Wire Line
	5950 4900 5950 4250
Wire Wire Line
	5950 3950 5200 3950
Wire Wire Line
	6650 4800 5750 4800
Wire Wire Line
	5750 4800 5750 4200
Wire Wire Line
	5750 4050 5200 4050
Wire Wire Line
	6650 5000 5650 5000
Wire Wire Line
	5650 5000 5650 4450
Wire Wire Line
	5650 3850 5200 3850
Wire Wire Line
	6650 5100 6050 5100
Wire Wire Line
	6050 3800 6650 3800
Wire Wire Line
	6050 3800 6050 4550
Connection ~ 6750 3800
Wire Wire Line
	6750 3800 6750 3900
$Comp
L power:GND #PWR0101
U 1 1 5E3B4224
P 4300 5100
F 0 "#PWR0101" H 4300 4850 50  0001 C CNN
F 1 "GND" H 4305 4927 50  0000 C CNN
F 2 "" H 4300 5100 50  0001 C CNN
F 3 "" H 4300 5100 50  0001 C CNN
	1    4300 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 5100 4300 4900
Connection ~ 4900 2800
$Comp
L power:+3.3V #PWR0102
U 1 1 5E3B6ECF
P 4900 2050
F 0 "#PWR0102" H 4900 1900 50  0001 C CNN
F 1 "+3.3V" H 4915 2223 50  0000 C CNN
F 2 "" H 4900 2050 50  0001 C CNN
F 3 "" H 4900 2050 50  0001 C CNN
	1    4900 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2050 4900 2150
$Comp
L power:VCC #PWR0103
U 1 1 5E3BCCCA
P 2200 1250
F 0 "#PWR0103" H 2200 1100 50  0001 C CNN
F 1 "VCC" H 2217 1423 50  0000 C CNN
F 2 "" H 2200 1250 50  0001 C CNN
F 3 "" H 2200 1250 50  0001 C CNN
	1    2200 1250
	1    0    0    -1  
$EndComp
$Comp
L CO2Mofetten:Adafruit_MicroSD U3
U 1 1 5E3A141D
P 7050 5150
F 0 "U3" H 7428 5396 50  0000 L CNN
F 1 "Adafruit_MicroSD" H 7428 5305 50  0000 L CNN
F 2 "CO2Mofetten:Adafruit_MicroSD_PinSocket" H 7050 5850 50  0001 C CNN
F 3 "" H 7050 5850 50  0001 C CNN
	1    7050 5150
	1    0    0    -1  
$EndComp
NoConn ~ 4400 3650
NoConn ~ 4400 3750
Connection ~ 4300 4900
Wire Wire Line
	4300 4900 4300 4550
Wire Wire Line
	3900 4900 4300 4900
Connection ~ 6050 4550
Wire Wire Line
	6550 2800 6550 3400
Wire Wire Line
	6550 3400 6800 3400
NoConn ~ 6650 5300
$Comp
L Regulator_Switching:TSR_1-2433 U1
U 1 1 5E420AF7
P 3900 2250
F 0 "U1" H 3900 2617 50  0000 C CNN
F 1 "TSR_1-2433" H 3900 2526 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_TRACO_TSR-1_THT" H 3900 2100 50  0001 L CIN
F 3 "http://www.tracopower.com/products/tsr1.pdf" H 3900 2250 50  0001 C CNN
	1    3900 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2150 4900 2150
Connection ~ 4900 2150
Wire Wire Line
	4900 2150 4900 2650
NoConn ~ 4700 2950
NoConn ~ 5200 3250
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 5E45774A
P 1700 2200
F 0 "J2" H 1618 1875 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 1618 1966 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 1700 2200 50  0001 C CNN
F 3 "~" H 1700 2200 50  0001 C CNN
	1    1700 2200
	-1   0    0    1   
$EndComp
Connection ~ 3900 2450
Wire Wire Line
	3900 2450 3900 2600
Wire Wire Line
	1900 2200 2200 2200
Wire Wire Line
	1900 2100 2100 2100
Wire Wire Line
	2100 2100 2100 2450
Wire Wire Line
	2200 1250 2200 2150
Wire Wire Line
	3500 2150 3400 2150
Connection ~ 2200 2150
Wire Wire Line
	2200 2150 2200 2200
Wire Wire Line
	6650 3800 6650 2950
Wire Wire Line
	6650 2950 7000 2950
Wire Wire Line
	7400 2950 7400 3100
Connection ~ 6650 3800
Wire Wire Line
	6650 3800 6750 3800
Wire Wire Line
	7300 2950 7300 3100
Connection ~ 7300 2950
Wire Wire Line
	7300 2950 7400 2950
Wire Wire Line
	7100 2950 7100 3100
Connection ~ 7100 2950
Wire Wire Line
	7100 2950 7300 2950
Wire Wire Line
	7000 2950 7000 3100
Connection ~ 7000 2950
Wire Wire Line
	7000 2950 7100 2950
Wire Wire Line
	4900 2950 4900 2800
$Comp
L MCU_Module:WeMos_D1_mini U2
U 1 1 5E39C2EF
P 4800 3750
F 0 "U2" H 4800 2861 50  0000 C CNN
F 1 "WeMos_D1_mini" H 4800 2770 50  0000 C CNN
F 2 "Module:WEMOS_D1_mini_light" H 4800 2600 50  0001 C CNN
F 3 "https://wiki.wemos.cc/products:d1:d1_mini#documentation" H 2950 2600 50  0001 C CNN
	1    4800 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3500 6350 3500
Wire Wire Line
	4900 2800 6550 2800
Wire Wire Line
	6500 3300 6500 4350
Wire Wire Line
	6500 5200 6650 5200
Wire Wire Line
	5850 3300 5850 2650
Wire Wire Line
	5850 2650 4900 2650
Connection ~ 4900 2650
Wire Wire Line
	4900 2650 4900 2800
Text Notes 5200 3350 0    20   ~ 0
This input needs to \nhave a PULLUP activated!
Wire Wire Line
	4300 4550 4800 4550
$Comp
L CO2Mofetten:Adafruit_DS3231 U4
U 1 1 5E41719C
P 7050 2000
F 0 "U4" H 7378 1996 50  0000 L CNN
F 1 "Adafruit_DS3231" H 7378 1905 50  0000 L CNN
F 2 "CO2Mofetten:Adafruit_DS3231_PinSocket" H 7050 2450 50  0001 C CNN
F 3 "https://learn.adafruit.com/adafruit-ds3231-precision-rtc-breakout/overview" H 7050 2450 50  0001 C CNN
	1    7050 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2000 6350 2000
Wire Wire Line
	6350 2000 6350 3500
Connection ~ 6350 3500
Wire Wire Line
	6350 3500 6800 3500
Wire Wire Line
	6700 1900 6400 1900
Wire Wire Line
	6400 1900 6400 3700
Connection ~ 6400 3700
Wire Wire Line
	6400 3700 5600 3700
Wire Wire Line
	6700 1800 5500 1800
Wire Wire Line
	5500 1800 5500 2600
Wire Wire Line
	5500 2600 3900 2600
Connection ~ 3900 2600
Wire Wire Line
	3900 2600 3900 4900
Wire Wire Line
	4900 2150 5850 2150
Wire Wire Line
	5850 2150 5850 1700
Wire Wire Line
	5850 1700 6700 1700
Connection ~ 4800 4550
Wire Wire Line
	4800 4550 6050 4550
NoConn ~ 6700 2100
NoConn ~ 6700 2200
NoConn ~ 6700 2300
NoConn ~ 6700 2400
NoConn ~ 5200 3650
NoConn ~ 5200 3750
NoConn ~ 4400 3350
Wire Wire Line
	2100 2450 3150 2450
$Comp
L Transistor_FET:IRLML9301 Q2
U 1 1 5E556407
P 3150 2250
F 0 "Q2" V 3492 2250 50  0000 C CNN
F 1 "IRLML9301" V 3401 2250 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 3350 2175 50  0001 L CIN
F 3 "https://www.infineon.com/dgdl/irlml9301pbf.pdf?fileId=5546d462533600a401535668e5e42640" H 3150 2250 50  0001 L CNN
	1    3150 2250
	0    -1   -1   0   
$EndComp
Connection ~ 3150 2450
Wire Wire Line
	3150 2450 3450 2450
Wire Wire Line
	2200 2150 2950 2150
Wire Wire Line
	5850 3300 6500 3300
NoConn ~ 5200 3350
Wire Wire Line
	6050 4550 6050 5100
Wire Wire Line
	5200 4150 6350 4150
Wire Wire Line
	6350 4150 6350 4700
Wire Wire Line
	6350 4700 6650 4700
$Comp
L power:+5V #PWR0104
U 1 1 5E66857F
P 4700 1450
F 0 "#PWR0104" H 4700 1300 50  0001 C CNN
F 1 "+5V" H 4715 1623 50  0000 C CNN
F 2 "" H 4700 1450 50  0001 C CNN
F 3 "" H 4700 1450 50  0001 C CNN
	1    4700 1450
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:TSR_1-2450 U5
U 1 1 5E668D26
P 3900 1550
F 0 "U5" H 3900 1917 50  0000 C CNN
F 1 "TSR_1-2450" H 3900 1826 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_TRACO_TSR-1_THT" H 3900 1400 50  0001 L CIN
F 3 "http://www.tracopower.com/products/tsr1.pdf" H 3900 1550 50  0001 C CNN
	1    3900 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2450 3450 1750
Wire Wire Line
	3450 1750 3900 1750
Connection ~ 3450 2450
Wire Wire Line
	3450 2450 3900 2450
Wire Wire Line
	3400 2150 3400 1450
Wire Wire Line
	3400 1450 3500 1450
Connection ~ 3400 2150
Wire Wire Line
	3400 2150 3350 2150
Wire Wire Line
	4300 1450 4700 1450
Wire Wire Line
	6800 3300 6700 3300
Wire Wire Line
	6700 3300 6700 3200
Wire Wire Line
	6700 3200 6050 3200
Wire Wire Line
	6050 3200 6050 1450
Wire Wire Line
	6050 1450 4700 1450
Connection ~ 4700 1450
NoConn ~ 6650 4600
Wire Wire Line
	7950 4650 7500 4650
Wire Wire Line
	7500 4650 7500 4250
Wire Wire Line
	7500 4250 5950 4250
Connection ~ 5950 4250
Wire Wire Line
	5950 4250 5950 3950
Wire Wire Line
	7950 4550 7600 4550
Wire Wire Line
	7600 4550 7600 4400
Wire Wire Line
	7600 4400 6650 4400
Wire Wire Line
	6650 4400 6650 3800
Wire Wire Line
	7950 4450 5650 4450
Connection ~ 5650 4450
Wire Wire Line
	5650 4450 5650 3850
Wire Wire Line
	7950 4350 6500 4350
Connection ~ 6500 4350
Wire Wire Line
	6500 4350 6500 5200
Wire Wire Line
	7950 4250 7650 4250
Wire Wire Line
	7650 4250 7650 4200
Wire Wire Line
	7650 4200 5750 4200
Connection ~ 5750 4200
Wire Wire Line
	5750 4200 5750 4050
NoConn ~ 7950 4750
$Comp
L CO2Mofetten:Sparkfun_MircoSD U6
U 1 1 5E66832F
P 8250 4450
F 0 "U6" H 8022 4409 50  0000 R CNN
F 1 "Sparkfun_MircoSD" H 8022 4500 50  0000 R CNN
F 2 "CO2Mofetten:SparkFun_MicroSD_PinSocket" H 8250 4850 50  0001 C CNN
F 3 "" H 8250 4850 50  0001 C CNN
	1    8250 4450
	-1   0    0    1   
$EndComp
Wire Wire Line
	7950 4150 6350 4150
Connection ~ 6350 4150
$EndSCHEMATC
